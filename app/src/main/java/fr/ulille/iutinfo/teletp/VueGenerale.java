package fr.ulille.iutinfo.teletp;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.navigation.fragment.NavHostFragment;

import java.util.prefs.Preferences;

public class VueGenerale extends Fragment {


    private String poste;
    private final String DISTANICEL=getResources().getStringArray(R.array.list_salles)[0];
    private String salle;

    private SuiviViewModel model;
    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.vue_generale, container, false);
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        poste = "";
        salle = DISTANICEL;

        model = new SuiviViewModel(requireActivity().getApplication());

        Spinner spSalle = (Spinner) getActivity().findViewById(R.id.spSalle);
        ArrayAdapter adaptersalle = ArrayAdapter.createFromResource(getContext(),
                R.array.list_salles, android.R.layout.simple_spinner_item);
        spSalle.setAdapter(adaptersalle);

        Spinner spPoste = (Spinner) getActivity().findViewById(R.id.spPoste);
        ArrayAdapter<CharSequence> adapterposte = ArrayAdapter.createFromResource(getContext(),
                R.array.list_postes, android.R.layout.simple_spinner_item);
        spPoste.setAdapter(adapterposte);


        view.findViewById(R.id.btnToListe).setOnClickListener(view1 -> {
            NavHostFragment.findNavController(VueGenerale.this).navigate(R.id.generale_to_liste);
            TextView username = getActivity().findViewById(R.id.tvLogin);
            model.setUsername(username.toString());
        });

        update(spPoste);


        // TODO Q9
    }
    public void update(Spinner spPoste ){
    if(salle==DISTANICEL) {
        spPoste.setVisibility(View.INVISIBLE);
        spPoste.setEnabled(false);
        model.setLocalisation(DISTANICEL);
    }else {
        spPoste.setEnabled(true);
        spPoste.setVisibility(View.VISIBLE);
        model.setLocalisation(salle+" : "+ spPoste.toString());
    }

    }
    // TODO Q9
}